---
## Systemadministraton 2 Übung
* SAM 2 Übung
* Devops

---
## Template
* Devops Template
* Version 20200323

---
## Agenda Übung Template
* Voraussetzungen vLAB
* Installation
* Einführung
* Umgebung vLAB
* First Steps
* Aufgaben
* Troubleshooting
* Cheatsheet
* Links
* Notizen

---
## Voraussetzungen vLAB
* Hypervisor Virtualbox / HyperV
* 2 x CPU Cores
* 4 GB RAM
* 2 GB Disk
* Linux Ubuntu 18.04

---
## Vorbereitungen vLAB
* Vorbereitungen vLAB Vagrant
* Vorbereitungen vLAB Manual (optional)

---
## Vorbereitungen vLAB (Vagrant)
* git clone https://www.gitlab.com/rstumper/sam-vlab-devops-puppet
* cd vlab/vagrant-virtualbox
* vagrant up
* vagrant ssh
* Username: vagrant
* Password: vagrant

---
## Vorbereitungen vLAB (Docker)
* git clone https://www.gitlab.com/rstumper/sam-vlab-devops-puppet
* cd vlab/docker-basis/
* docker compose up
* docker build
* docker run -it sam-vlab-devops-puppet
* Username: vagrant
* Password: vagrant

---
# Einführung Puppet
* Ist ein Framework um Software Auszurollen
* Desired State Configuration (Make it so)
* Declerative DSL
* Based on Ruby
* Client / Server Architektur

---
## Installation Puppet v4.8(Manual)
* Ubuntu 16.04 LTS Basis
* Install Puppet Agent:
	* wget https://apt.puppetlabs.com/puppetlabs-release-pc1-wheezy.deb
	* dpkg -i puppetlabs-release-pc1-wheezy.deb
	* apt-get update
	* apt-get install puppet-agent


---
## Troubleshooting Hyper-V
#### Um Vagrant mit VirtualBox und Installiertem Hyper-V zu betreiben ist es notwendig Hyper-V zu deaktivieren.

Powershell:
```
Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
```

---
## First Steps in Puppet
* Check Puppet Agent Version:
	* /opt/puppetlabs/bin/puppet agent --version

---
## First Steps in Puppet
* Puppet Resources

---
## Troubleshooting
* Troubleshooting Vagrant and Hyper-V

---
## Troubleshooting Hyper-V
#### Um Vagrant mit VirtualBox und Installiertem Hyper-V zu betreiben ist es notwendig Hyper-V zu deaktivieren.

* Turn Hyper-V OFF
	* bcdedit /set hypervisorlaunchtype off

* Turn Hyper-V OFF
	* bcdedit /set hypervisorlaunchtype auto

---
# Cheat Sheet
* Installation (sudo apt-get install snapd snap-confine )
* Snap Version (sudo snap --version)
* Snap Package Suchen ( snap find )
* Snap Package Installieren (sudo snap install )
* snap list
* snap refresh
* snap remove
* snap info

---
# Links
* https://wiki.ubuntuusers.de/snap/
* https://snapcraft.io/docs/core/usage
* https://uappexplorer.com
* https://www.ubuntu.com/desktop/snappy

---
# Begriffserklärungen
* Domain Specific Language (DSL)
    * https://de.wikipedia.org/wiki/Dom%C3%A4nenspezifische_Sprache

---
# Notizen
