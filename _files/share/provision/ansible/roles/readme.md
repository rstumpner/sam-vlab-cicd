# The Place for Ansible Roles


## Option 1
change the included Role to install the requirements:

- cd _files/share/provision/roles/vlab/
#### Update Packages to Install
- Edit /home/roland/ownCloud/lehre/vlab/sam-vlab-template/_files/share/provision/ansible/roles/vlab/tasks/install.yml

#### Local Testing with Molecule
- install molecule
- cd _files/share/provision/ansible/roles/vlab
- molecule test
#### change the role name in the Provision File 
```
# _files/share/provision/ansible/provision.yml

- hosts: all
  become: yes
  roles:
     - vlab
```

## Option 2
Install an Ansible Galaxy Module:

- cd _files/share/provision/roles
- Install a Role via Ansible Galaxy ´ansible-galaxy install --roles-path . geerlingguy.nginx`
- add the role to the Provision File 
```
# _files/share/provision/ansible/provision.yml

- hosts: all
  become: yes
  roles:
     - geerlingguy.nginx
```

## Option 3
Install an Ansible Role via Git
`git clone https://gitlab.com/rstumpner/ansible-role-template-linux.git role-vlab`

## Option 4
Install Ansible Roles via requirements.yml file
`ansible-galaxy install -r roles/requirements.yml`
